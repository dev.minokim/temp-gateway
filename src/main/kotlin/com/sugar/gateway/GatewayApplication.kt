package com.sugar.gateway

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@SpringBootApplication
@EnableDiscoveryClient
class GatewayApplication

fun main(args: Array<String>) {
	System.setProperty("reactor.netty.http.server.accessLogEnabled", "true")
	runApplication<GatewayApplication>(*args)
}
